<?php
	
	class ConsoleController extends BaseController{


		public function index(){
			return View::make('console');
		}

		public function list_column($table){
			// $db_list = DB::getSchemaBuilder()->getColumnListing($table);
			// return Response::json($db_list);
			// $table_info_columns = DB::select( DB::raw('SHOW COLUMNS FROM '.$table));
			// $table_details = array();
			// $i=0;
			//   foreach($table_info_columns as $column){
			//     $col_name = $column->Field;
			//     $col_type = $column->Type;
			//     // var_dump($col_name,$col_type);
			//     $table_details[$i]['field'] = $col_name; 
			//     $table_details[$i]['type'] = $col_type; 
			//     $i++;
			//   } 

			// return Response::json($table_details);  
			$col_list = FormSetting::where('table','=',$table)->get();
			return Response::json($col_list);
		}

		public function add_column($table){
			Schema::table($table,function($t){
				$type = Input::get('type');
				if($type == 'varchar(255)'){
					$t->string(Input::get('field'));
				} else if($type == 'int(11)' || $type == 'int(10) unsigned'){
					$t->integer(Input::get('field'));
				} else if($type == 'text'){
					$t->text(Input::get('field'));
				} else if($type == 'datetime'){
					$t->datetime(Input::get('field'));	
				} else if($type == 'timestamp'){
					$t->timestamp(Input::get('field'));	
				} else {
					$t->integer(Input::get('field'));
				}
			});	

			$fs = new FormSetting;
			$fs->table = $table;
			$fs->column = Input::get('field');
			$fs->type = Input::get('type');
			$fs->form_type = Input::get('form_type');
			$fs->save();

			$col_list = FormSetting::where('table','=',$table)->get();
			return Response::json($col_list);
		}

		public function update_column($table){

			$col = FormSetting::where('table','=',$table)->where('column','=',Input::get('from'))->first(); 
			Schema::table($table,function($t){
				$t->renameColumn(Input::get('from'),Input::get('to'));
			});

			DB::statement('ALTER TABLE '.$table.' MODIFY COLUMN '.Input::get('to').' '.Input::get('type'));

			
			$col->column = Input::get('to');
			$col->type = Input::get('type');
			$col->form_type = Input::get('form_type');
			$col->save();

			$col_list = FormSetting::where('table','=',$table)->get();
			return Response::json($col_list);
		}

		public function drop_column($table){
			Schema::table($table,function($t){
				$t->dropColumn(Input::get('field'));
			});

			$table_info_columns = DB::select( DB::raw('SHOW COLUMNS FROM '.$table));
			$table_details = array();
			$i=0;
			  foreach($table_info_columns as $column){
			    $col_name = $column->Field;
			    $col_type = $column->Type;
			    // var_dump($col_name,$col_type);
			    $table_details[$i]['field'] = $col_name; 
			    $table_details[$i]['type'] = $col_type; 
			    $i++;
			  } 

			return Response::json($table_details);
		}

	}

?>