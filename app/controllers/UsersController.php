<?php
	
	class UsersController extends BaseController {
		
		public function index(){
			$data['users'] = User::all();
			$data['forms'] = FormSetting::where('table','=','users')->get();
			return View::make('users.index',$data);
		}

	}

?>