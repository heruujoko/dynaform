<?php
	
	class SetupTableSeeder extends Seeder {
		public function run(){
			$table_info_columns = DB::select( DB::raw('SHOW COLUMNS FROM users'));
			$table_details = array();
			$i=0;
			  foreach($table_info_columns as $column){
			    $col_name = $column->Field;
			    $col_type = $column->Type;
			    if($col_name != 'created_at' && $col_name != 'updated_at'){
			    	$forms = new FormSetting;
				    $forms->table = 'users';
				    $forms->column = $col_name;
				    $forms->type = $col_type;
				    $forms->form_type = 'text';
				    $forms->save();  
			    }
			  }		
		}
	}

?>