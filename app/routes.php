<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	// return View::make('hello');
	return Redirect::to('console');
});
Route::get('/console','ConsoleController@index');
Route::get('/console/{table}', 'ConsoleController@list_column');
Route::post('/console/{table}/add', 'ConsoleController@add_column');
Route::post('/console/{table}/update', 'ConsoleController@update_column');
Route::post('/console/{table}/drop', 'ConsoleController@drop_column');

Route::get('users','UsersController@index');
