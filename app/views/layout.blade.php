<!DOCTYPE html>
<html>
<head>
	<script src="{{ URL::to('/js/application.js') }}"></script>
	<script src="{{ URL::to('/js/factory/ConsoleFactory.js') }}"></script>
	<script src="{{ URL::to('/js/controller/consoleController.js') }}"></script>
	<title>Database Console</title>

	<link rel="stylesheet" type="text/css" href="{{ URL::to('/themes/css/bootstrap.min.css') }}">
	<link href="{{ URL::to('/themes/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ URL::to('/themes/css/animate.css') }}" rel="stylesheet">
    <link href="{{ URL::to('/themes/css/style.css') }}" rel="stylesheet">
    <style type="text/css">
    	.modal{
    		left: 40%;
    		top: 10%;
    	}
    </style>
</head>
<body>

<div id="wrapper">

    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="{{ URL::to('/themes/img/profile_small.jpg') }}" />
                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">David Williams</strong>
                             </span> <span class="text-muted text-xs block">Art Director <b class="caret"></b></span> </span> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href="profile.html">Profile</a></li>
                            <li><a href="contacts.html">Contacts</a></li>
                            <li><a href="mailbox.html">Mailbox</a></li>
                            <li class="divider"></li>
                            <li><a href="login.html">Logout</a></li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        IN+
                    </div>
                </li>
                <li><a href="{{ URL::to('/console') }}"><i class="fa fa-laptop"></i> <span class="nav-label">Console</span> </a></li>
                <li>
                    <a href="index.html"><i class="fa fa-th-large"></i> <span class="nav-label">Table</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{ URL::to('/users') }}">Users</a></li>
                        
                    </ul>
                </li>
            </ul>

        </div>
    </nav>
        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top  " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <form role="search" class="navbar-form-custom" action="search_results.html">
                <div class="form-group">
                    <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                </div>
            </form>
        </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <a href="login.html">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li>
            </ul>

        </nav>
        </div>
        	<div id="main">
        		@yield('content')
            </div>
            <div class="footer">
                <div class="pull-right">
                    10GB of <strong>250GB</strong> Free.
                </div>
                <div>
                    <strong>Copyright</strong> Example Company &copy; 2014-2015
                </div>
            </div>

        </div>
        </div>



    <script type="text/javascript" src="{{ URL::to('/themes/js/jquery-2.1.1.js') }}"></script>
    <script type="text/javascript" src="{{ URL::to('/themes/js/bootstrap.min.js') }}"></script>    
    <script type="text/javascript" src="{{ URL::to('/themes/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
    <script type="text/javascript" src="{{ URL::to('/themes/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>    
    <script type="text/javascript" src="{{ URL::to('/themes/js/inspinia.js') }}"></script>
</body>
</html>