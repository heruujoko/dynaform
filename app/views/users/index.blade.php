@extends('layout')

@section('content')
<div class="row">
	<div class="col-md-12">
		<h2>Users</h2>	
	</div>
</div>
<div class="row">
	<div class="col-md-12">	
		<div class="panel panel-default">
			<div class="panel-heading">Users Form</div>
			<div class="panel-body">
				<form class="form form-horizontal">
					@foreach($forms as $item)
						<div class="form-group">
							<label class="control-label col-md-2">{{ $item->column }}</label>
							<div class="col-md-8">
								@if($item->form_type == 'field')
									<input type="text" name="{{ $item->column }}" class="form-control"/>
								@elseif($item->form_type == 'number')
									<input type="number" name="{{ $item->column }}" class="form-control"/>
								@elseif($item->form_type == 'date')
									<input type="date" name="{{ $item->column }}" class="form-control"/>
								@else
								@endif
							</div>
						</div>
					@endforeach
				</form>
			</div>
		</div>
	</div>
</div>
@endsection