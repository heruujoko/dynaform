var dynaform = angular.module('dynaform', ['ngRoute']);

dynaform.config(function($routeProvider){
	$routeProvider

	.when('/',{
		templateUrl: 'angularpages/console_list.html',
		controller: 'consoleController' 
	})
	.otherwise({ redirectTo: '/' });
});