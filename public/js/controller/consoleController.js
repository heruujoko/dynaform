dynaform.controller('consoleController',function($scope,consoleFactory){
	$scope.section = "Users Table";
	$scope.newItem = {
		type: 'int(11)',
		form_type: 'field'
	};
	$scope.typeList = [
		'int(11)',
		'int(10) unsigned',
		'varchar(255)',
		'text',
		'datetime',
		'timestamp'
	];
	$scope.formTypeList = [
		'field',
		'date',
		'number'
	];


	$scope.addItem = function(){
		consoleFactory.addColumn('users',$scope.newItem)
		.then(function(response){
			$scope.columns = response.data;
			$('#addmodal').modal('hide');
		},function(error){
			$scope.status = "error application";
		});
	}

	$scope.editItem = function(item){
		$scope.oldItem = item.column;
		$scope.editedItem = item;
	}

	$scope.updateItem = function(){
		consoleFactory.updateColumn('users',$scope.oldItem,$scope.editedItem)
		.then(function(response){
			$scope.columns = response.data;
			$('#editmodal').modal('hide');
		},function(error){
			$scope.status = "error application";	
		})
	}

	$scope.dropItem = function(field){
		consoleFactory.dropColumn('users',field)
		.then(function(response){
			$scope.columns = response.data;
		},function(error){});
	}

	consoleFactory.getColumns()
	.then(function(response){
		$scope.columns = response.data;
	},function(error){
		$scope.status = "error application";
	})
});