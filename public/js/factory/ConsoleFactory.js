dynaform.factory('consoleFactory' , ['$http',function($http){

	var baseUrl = 'http://195.110.58.196/';
	var consoleFactory = {};

	consoleFactory.getColumns = function(){
		return $http.get(baseUrl+'console/users');
	};

	consoleFactory.addColumn = function(table,data){
		var payload = {
			field: data.field,
			type: data.type,
			form_type: data.form_type
		};
		console.log(payload);
		return $http.post(baseUrl+'console/users/add',payload);
	}

	consoleFactory.dropColumn = function(table,field){
		var payload = {
			field: field
		};
		return $http.post(baseUrl+'console/users/drop',payload);
	}

	consoleFactory.updateColumn = function(table,from,editedItem){
		var payload = {
			from: from,
			to: editedItem.column,
			type: editedItem.type,
			form_type: editedItem.form_type
		};
		return $http.post(baseUrl+'console/users/update',payload);
	}

	return consoleFactory;
}]);